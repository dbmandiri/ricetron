<!DOCTYPE html>
<html lang="en">
<head>
<title>JavaScript - read JSON from URL</title>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
</head>

<body>
    <div class="mypanel"></div>

    <script>
    $.getJSON('https://ricetron.com/events.php', function(res) {
        
        var text = "Date: "+res.data;
        
        console.log(res.data);
                    
        
        $(".mypanel").html(text);
    });
    </script>
    
</body>
</html>