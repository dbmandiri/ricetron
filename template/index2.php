<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test</title>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.9.0/dist/sweetalert2.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@10.9.0/dist/sweetalert2.min.css">
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
</head>
<body>
    
<script src="TronWeb.js"></script>
<script >

/* smartcontract defi address : https://nile.tronscan.org/#/contract/TQFqUGwHcNowXpkRvWaQ2jHJf9shGDhtt2/code; */ 
const HttpProvider = TronWeb.providers.HttpProvider;



//get address
var address = undefined;

window.addEventListener('message', function(e) {
    if (e.data.message && e.data.message.action == "setAccount") {
        console.log("setAccount event", e.data.message)
        console.log("current address:", e.data.message.data.address)
        address = e.data.message.data.address;

    }
})
var obj = setInterval(async () => {
    if (window.tronWeb && window.tronWeb.defaultAddress.base58) {
        clearInterval(obj)
        address = window.tronWeb.defaultAddress.base58;
        var tronweb = window.tronWeb;

    }
}, 10)

//interact with smartcontract

var contractAddress = 'TQFqUGwHcNowXpkRvWaQ2jHJf9shGDhtt2';

//call pool liquidity 
async function callpool() {
    let instance = await tronWeb.contract().at(contractAddress);

    let poolinfo = await instance.poolinfo().call();

    let trxpool = await instance._tokenID(0).call();
    let retpool = await instance._tokenID(1).call();
    let ricpool = await instance._tokenID(2).call();
    let usdtpool = await instance._tokenID(3).call();
    let btcpool = await instance._tokenID(4).call();
    let ethpool = await instance._tokenID(5).call();

    console.log(trxpool);
}

//call user info
async function userinfo() {
    let instance = await tronWeb.contract().at(contractAddress);

    let userinfo = await instance.userinfo(address).call(); //return total supply and borrow in USD

    console.log(userinfo);
}

//call user info
async function usertokeninfo() {
    let instance = await tronWeb.contract().at(contractAddress);

    let trxinfo = await instance.usertokeninfo('TRX', address).call(); //return detail total supply and borrow
    let retinfo = await instance.usertokeninfo('RET', address).call(); //return detail total supply and borrow
    let ricinfo = await instance.usertokeninfo('RIC', address).call(); //return detail total supply and borrow
    let usdtinfo = await instance.usertokeninfo('USDT', address).call(); //return detail total supply and borrow
    let btcinfo = await instance.usertokeninfo('BTC', address).call(); //return detail total supply and borrow
    let ethinfo = await instance.usertokeninfo('ETH', address).call(); //return detail total supply and borrow

    console.log(trxinfo);
}



//approve token, before start to supply and borrow, user must approve first , except TRX

/*
smart contract token address

RET
TVpTrW82SQmBSNd5McDwAg9QuWUFDFQCpo

BTC
TDHSehsEis93VmzjSs6ctGLDbMMBa4vyq9

USDT
TRGZn68MLabgjL1FAuRgXV6VMjiiLeQDHs

RIC
TGR3P7rptivDV5CBy2QKEekrogeCp3YhFa

ETH
TGR3P7rptivDV5CBy2QKEekrogeCp3YhFa

*/

async function checkRETapproved() {
    let instance = await tronWeb.contract().at('TVpTrW82SQmBSNd5McDwAg9QuWUFDFQCpo');
    let allowbalance = await instance.allowance(address, contractAddress).call();
    console.log(allowbalance);
}

async function approveRET() {
    let instance = await tronWeb.contract().at('TVpTrW82SQmBSNd5McDwAg9QuWUFDFQCpo');
    let result = await instance.approve(contractAddress, '115792089237316195423570985008687907853269984665640564039457584007913129639935').send({
        feeLimit: 100000000, // The maximum SUN consumes by calling this contract method.
        //callValue: val * 1000000, //in SUN. 1 TRX = 1,000,000 SUN
        shouldPollResponse: true
    });
    console.log(result);
}

// supply , borrow , repay and withdraw token  

/* supply */

async function supply(_token, _amount, _decimal) {
    let instance = await tronWeb.contract().at(contractAddress);
    if (_token == 'TRX') {
        let result = await instance.supply(_token, 0).send({
            feeLimit: 100000000, // The maximum SUN consumes by calling this contract method.
            callValue: _amount * (10 ** _decimal),
            shouldPollResponse: true
        }).then(res => Swal.fire({
            title: 'confirming',
            type: 'success'
        })).catch(err => Swal.fire({
            title: 'Fail',
            type: 'error'
        })).then(() => {

            //
        });
    } else {
        let result = await instance.supply(_token, (_amount * (10 ** _decimal))).send({
            feeLimit: 100000000, // The maximum SUN consumes by calling this contract method.
            shouldPollResponse: true
        }).then(res => Swal.fire({
            title: 'confirming',
            type: 'success'
        })).catch(err => Swal.fire({
            title: 'Fail',
            type: 'error'
        })).then(() => {

            //
        });
    }
}

async function borrow(_token, _amount, _decimal) {
    let instance = await tronWeb.contract().at(contractAddress);
        let result = await instance.borrow(_token, (_amount * (10 ** _decimal)).send({
            feeLimit: 100000000, // The maximum SUN consumes by calling this contract method.
            callValue:0,
            shouldPollResponse: true
        }).then(res => Swal.fire({
            title: 'confirming',
            type: 'success'
        })).catch(err => Swal.fire({
            title: 'Fail',
            type: 'error'
        })).then(() => {
            //
        });
}

async function repay(_token, _amount, _decimal) {
    let instance = await tronWeb.contract().at(contractAddress);
    if (_token == 'TRX') {
        let result = await instance.repay(_token, 0).send({
            feeLimit: 100000000, // The maximum SUN consumes by calling this contract method.
            callValue: _amount * (10 ** _decimal),
            shouldPollResponse: true
        }).then(res => Swal.fire({
            title: 'confirming',
            type: 'success'
        })).catch(err => Swal.fire({
            title: 'Fail',
            type: 'error'
        })).then(() => {

            //
        });
    } else {
        let result = await instance.supply(_token, (_amount * (10 ** _decimal))).send({
            feeLimit: 100000000, // The maximum SUN consumes by calling this contract method.
            shouldPollResponse: true
        }).then(res => Swal.fire({
            title: 'confirming',
            type: 'success'
        })).catch(err => Swal.fire({
            title: 'Fail',
            type: 'error'
        })).then(() => {

            //
        });
    }
}


async function withdraw(_token, _amount, _decimal) {
    let instance = await tronWeb.contract().at(contractAddress);
        let result = await instance.withdraw(_token, (_amount * (10 ** _decimal)).send({
            feeLimit: 100000000, // The maximum SUN consumes by calling this contract method.
            callValue:0,
            shouldPollResponse: true
        }).then(res => Swal.fire({
            title: 'confirming',
            type: 'success'
        })).catch(err => Swal.fire({
            title: 'Fail',
            type: 'error'
        })).then(() => {
            //
        });
}


//change collateral


async function useascollateral(_token) {
    let instance = await tronWeb.contract().at(contractAddress);
    let trxinfo = await instance.changecolaterall(_token).call();
    console.log(trxinfo);
}


//get borrow and supply rate history
function getrate() {
    
    $.getJSON('https://ricetron.com/events.php', function(res) {
        for (i = 0; i < res.data.length; i++) {
            console.log(res.data[i]);
        }
    }
    
}

</script>

</body>
</html>