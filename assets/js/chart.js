const HttpProvider = TronWeb.providers.HttpProvider;
const fullNode = new HttpProvider("https://api.trongrid.io");
const solidityNode = new HttpProvider("https://api.trongrid.io");
const eventServer = new HttpProvider("https://api.trongrid.io");

var NoUp = 'TQuAji4sB1Fzpuj3dQH8ABCqLiiNBu6nu4';
var contractAddress = 'TPBRQ9AqBpKctTyJ3RmAcobW8cNAFXF4SB';
var params = new URLSearchParams(window.location.search);
var refid = params.get('ref');
var upline = refid;
var trx_bal = 0;
var ric_bal = 0;
if (refid !== null) {
    upline = refid;
} else {
    upline = NoUp;
}

var allevent = [];
var Address = {};

function inittronlink() {
    if (window.tronWeb && window.tronWeb.defaultAddress.base58) {
        Address.address = window.tronWeb.defaultAddress.base58;
        //alert(Address.address.hex[4]);
        let myadr = Address.address.slice(0, 5) + "xxx" + Address.address.slice(29, 34);
        document.getElementById("MyAddress").innerHTML = myadr;
    }
}
setInterval(myMethod1, 3000);

function myMethod1() {
    inittronlink();
    gethex();
    allowance();
}
setInterval(myMethod2, 10000);

function myMethod2() {
    userinfo();
    stats();
    filter_event();
    usablebal();

}
getev();
async function getBalance() {
    const bal = await tronWeb.trx.getBalance(Address.address);
    Address.balance = (bal / 1000000).toFixed(2);
    trx_bal = Address.balance;
    document.getElementById("mybalance").innerHTML = Address.balance + " TRX";
}
var lastclaim = 0;
var RETBAL = 0;
async function userinfo() {
    let instance = await tronWeb.contract().at(contractAddress);
    let userinfo = await instance.user().call();
    let txt = JSON.stringify(userinfo);
    let parse = JSON.parse(txt);
    let active_stake = tronWeb.fromSun(tronWeb.toDecimal(parse[0]._hex));
    lastclaim = tronWeb.address.fromHex(parse[1]);
    if (lastclaim != 0 || lastclaim > 0) {
        document.getElementById("refflink").innerHTML = "https://ricetron.com/?ref=" + Address.address;
        document.getElementById("linknoted").innerHTML = "Your Link , Click to copy !";
    }
    let referrer = tronWeb.address.fromHex(parse[3]);
    let referralReward = tronWeb.fromSun(tronWeb.toDecimal(parse[4]._hex) / 1000000);
    let totalReferrals = tronWeb.toDecimal(parse[5]._hex);
    let pendingRewards = (tronWeb.fromSun(tronWeb.toDecimal(parse[6]._hex) / 1000000) * 1).toFixed(8);
    let tokenBalance = (tronWeb.fromSun(tronWeb.toDecimal(parse[7]._hex) / 1000000) * 1).toFixed(8);
    RETBAL = tronWeb.toDecimal(parse[7]._hex);
    let balance = tronWeb.fromSun(tronWeb.toDecimal(parse[8]._hex));
    document.getElementById("mybalance").innerHTML = +balance + "TRX";
    document.getElementById("mytokenbalance").innerHTML = tokenBalance + "RET";
    document.getElementById("RETSTAKING").innerHTML = tokenBalance + "RET";
    document.getElementById("swap-button").innerHTML = "Swap "+ tokenBalance + " RET";
    document.getElementById("myreward").innerHTML = referralReward + "RET";
    document.getElementById("myreferral").innerHTML = totalReferrals + " Referrals";
    document.getElementById("activestake").innerHTML = active_stake + "TRX";

    document.getElementById("readyclaim").innerHTML = pendingRewards + "RET";
}

var myarray = [];
var myreffarray = [];
var ricprice = 0;
var currentLevel = 0;
async function stats() {
    let instance = await tronWeb.contract().at(contractAddress);
    let userinfo = await instance.stats().call();
    let txt = JSON.stringify(userinfo);
    let parse = JSON.parse(txt);
    currentLevel = tronWeb.toDecimal(parse.currentLevel._hex);
    let currentLevelYield = tronWeb.toDecimal(parse.currentLevelYield._hex) / 100; // price per trx 
    ricprice = currentLevelYield / 10000000000;
    let currentLevelSupply = tronWeb.fromSun(tronWeb.toDecimal(parse.currentLevelSupply._hex)) / 1000000;
    let mintedCurrentLevel = tronWeb.fromSun(tronWeb.toDecimal(parse.mintedCurrentLevel._hex)) / 1000000;
    let totalRET = tronWeb.fromSun(tronWeb.toDecimal(parse.totalRET._hex)) / 1000000;
    let totalStaked = tronWeb.fromSun(tronWeb.toDecimal(parse.totalStaked._hex));
    document.getElementById("retprice").innerHTML = ricprice + "TRX";
    document.getElementById("totalstake").innerHTML = totalStaked + "TRX";
    document.getElementById("mininglevel").innerHTML = (currentLevel + 1) + " / 7";
    //console.log(totalStaked);    


}


function stakerecord() {
    var staketable = makeTableHTML(sort_array(myarray));
    document.getElementById("mystakehistory").innerHTML = staketable;

}

function reffrecord() {
    var refftable = makeTableHTML(sort_array(myreffarray));
    document.getElementById("myreffhistory").innerHTML = refftable;
}

function makeTableHTML(myArray) {
     let result = '';
    if(myArray.length <1){
       result = 'No history found'; 
    }else{
    result += '<table id="mytab" class="table table-borderless table-responsive"><thead><tr><th scope="col">#</th><th scope="col">type</th><th scope="col">Date</th><th scope="col">Amount</th></tr></thead><tbody><tr>';
    
    for (var i = 0; i < myArray.length; i++) {
        result += "<tr>";
        let numb = i + 1;
        result += "<td>" + numb + "</td>";
        let tbody = myArray[i];
        let Opr = tbody.event_name;
        if (Opr == 'Operation') {
            Opr = tbody.result._type;
            result += "<td>" + Opr + "</td>";
            result += "<td>" + timetodate(tbody.block_timestamp) + "</td>";
            result += "<td>" + (tbody.result._amount/1000000) + "TRX</td>";
        } else if (Opr == 'ReferralReward') {
            result += "<td>" + Opr + "</td>";
            result += "<td>" + timetodate(tbody.block_timestamp) + "</td>";
            result += "<td>" + (tronWeb.fromSun(tbody.result._amount) / 1000000 * 7).toFixed(9) + "RET</td>";
        } else {
            result += "<td>" + Opr + "</td>";
            result += "<td>" + timetodate(tbody.block_timestamp) + "</td>";
            result += "<td>" + (tronWeb.fromSun(tbody.result._amount) / 1000000).toFixed(9) + "RET</td>";
        }



        result += "</tr>";
    }
   
    result += "</table>";
    }
    return result;
}

function timetodate(t) {
    return moment(t).format("DD-MM-YYYY h:mm:ss");
}
stats();

function tohex(s) {
    return tronWeb.toHex(s);
}



function uniq(data, key) {
    return [
        ...new Map(
            data.map(x => [key(x), x])
        ).values()
    ]
}

function stakecalc(val) {
    let onday = ricprice * val;
    let onhour = onday / 24;
    var onweek = onday * 10;
    let minig_duration = currentLevel * 1;

    if (currentLevel < 1) {
        onweek = onday * 7;
    }
    console.log(currentLevel);
    document.getElementById("onhour").innerHTML = onhour.toFixed(8) + " RET";
    document.getElementById("onday").innerHTML = onday.toFixed(8) + " RET";
    document.getElementById("onmax").innerHTML = onweek.toFixed(8) + " RET";
}

function stake() {
    Swal.fire({
        title: 'Info',
        text: "Transaction fee up to 60 TRX applies. To reduce transaction fees, you can freeze some TRX to get energy.",
        icon: 'info',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Continue Stake!'
    }).then((result) => {
        if (result.isConfirmed) {
            let depo = true;
    if (window.tronWeb && window.tronWeb.defaultAddress.base58) {
        var obj = setInterval(async () => {
            clearInterval(obj);
            val = document.getElementById("trxamount").value;
            if (val < 100 || val === null) {
                amounterror();
                depo = false;
            } else {
                let instance = await tronWeb.contract().at(contractAddress);
                let result = await instance.stake(upline).send({
                    feeLimit: 100000000, // The maximum SUN consumes by calling this contract method.Hard capped at 1000 TRX.(1TRX = 1,000,000SUN)
                    callValue: val * 1000000, //in SUN. 1 TRX = 1,000,000 SUN
                    shouldPollResponse: true
                });
                console.log(result);
            }
        }, 10)
    } else {
        signin();
    }
        }
    })
    
}
function unstake() {
    Swal.fire({
        title: 'Please read first',
        text: "*please claim your RET first, before UNSTAKE your TRX. If not you will lose your RET.         unstake will be charge 10% fee from your staking",
        icon: 'info',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Yes, Unstake!'
    }).then((result) => {
        if (result.isConfirmed) {
            let depo = true;
            if (window.tronWeb && window.tronWeb.defaultAddress.base58) {
                var obj = setInterval(async () => {
                    clearInterval(obj);
                    let instance = await tronWeb.contract().at(contractAddress);
                    let result = await instance.unstake().send({
                        feeLimit: 100000000, // The maximum SUN consumes by calling this contract method.Hard capped at 1000 TRX.(1TRX = 1,000,000SUN)
                        //callValue:val*1000000,//in SUN. 1 TRX = 1,000,000 SUN
                        shouldPollResponse: true
                    });
                    console.log(result);

                }, 10)
            } else {
                signin();
            }
        }
    })
}

function claim() {

    if (window.tronWeb && window.tronWeb.defaultAddress.base58) {
        var obj = setInterval(async () => {
            clearInterval(obj);
            let instance = await tronWeb.contract().at(contractAddress);
            let result = await instance.claimStaked().send({
                feeLimit: 100000000, // The maximum SUN consumes by calling this contract method.Hard capped at 1000 TRX.(1TRX = 1,000,000SUN)
                //callValue:val*1000000,//in SUN. 1 TRX = 1,000,000 SUN
                shouldPollResponse: true
            });

            console.log(result);
        }, 10)
    } else {
        signin();
    }

}

function claimreffreward() {

    if (window.tronWeb && window.tronWeb.defaultAddress.base58) {
        var obj = setInterval(async () => {
            clearInterval(obj);
            let instance = await tronWeb.contract().at(contractAddress);
            let result = await instance.claimReferralReward().send({
                feeLimit: 100000000, // The maximum SUN consumes by calling this contract method.Hard capped at 1000 TRX.(1TRX = 1,000,000SUN)
                //callValue:val*1000000,//in SUN. 1 TRX = 1,000,000 SUN
                shouldPollResponse: true
            });
            Swal.fire(
                'Referral Reward Claimed!',
                'Referral successfully added to your wallet.',
                'success'
            )
        }, 10)
    } else {
        signin();
    }

}

function amounterror() {
    Swal.fire({
        title: 'Error',
        text: 'Amount must be more or equal to 100 TRX.',
        icon: 'error',
        confirmButtonText: 'close'
    });
}

function signin() {
    if (window.tronWeb && window.tronWeb.defaultAddress.base58) {} else {
        Swal.fire({
            title: 'Connection Request',
            text: "Please login with your Tronlink to connect Ricetron.",
            icon: 'warning',
            confirmButtonText: 'close'
        });
    }
}

function copylink() {
    if (lastclaim != 0) {
        let reflink = "https://ricetron.com/?ref=" + Address.address;
        copyToClipboard(reflink);
        Swal.fire({
            title: 'Success',
            text: 'Referral link was copied to clipboard.',
            icon: 'success',
            confirmButtonText: 'Close'
        });
    } else {
        Swal.fire({
            title: 'NO LINK',
            text: 'Please stake TRX first.',
            icon: 'warning',
            confirmButtonText: 'Close'
        });
    }

}

function copyToClipboard(text) {
    var input = document.body.appendChild(document.createElement("input"));
    input.value = text;
    input.select();
    document.execCommand('copy');
    input.parentNode.removeChild(input);
};

function gethex() {
    let url = 'hello.php?address=' + Address.address;

    let res = $.get(url);
    $.get(url, function(data) {

        Address.uniq = "0x" + data.slice(2, 42);

        //console.log(Address.uniq);

    });

}



function getev() {
    allevent = [];
    $.getJSON('https://ricetron.com/events.php?type=operation', function(res) {

        for (i = 0; i < res.data.length; i++) {
            allevent.push(res.data[i]);
        }

    });
    $.getJSON('https://ricetron.com/events.php?type=claim', function(res2) {
        for (i = 0; i < res2.data.length; i++) {
            allevent.push(res2.data[i]);
        }
    });
    $.getJSON('https://ricetron.com/events.php?type=reff', function(res3) {
        for (i = 0; i < res3.data.length; i++) {
            allevent.push(res3.data[i]);
        }
    });
    $.getJSON('https://ricetron.com/events.php?type=claimreff', function(res4) {
        for (i = 0; i < res4.data.length; i++) {
            allevent.push(res4.data[i]);
        }
    });

    filter_event();

}

function sort_array(array) {
    return array.sort((x, y) => +new Date(y.block_timestamp) - +new Date(x.block_timestamp));
}

function filter_event() {

    let myevent = allevent.sort((x, y) => +new Date(y.block_timestamp) - +new Date(x.block_timestamp));

    let stake = myevent.filter(function(hero) {
        return hero.result._type == "stake";
    });
    let mystake = stake.filter(function(hero) {
        return hero.result._user == (Address.uniq).toLowerCase();
    });

    let unstake = myevent.filter(function(hero) {
        return hero.result._type == "unstake";
    });

    let myunstake = unstake.filter(function(hero) {
        return hero.result._user == (Address.uniq).toLowerCase();
    });

    let reffrew = myevent.filter(function(hero) {
        return hero.event_name == "ReferralReward";
    });

    let myreferralreward = reffrew.filter(function(hero) {
        return hero.result._referrer == (Address.uniq).toLowerCase();
    });


    let claimreferral = myevent.filter(function(hero) {
        return hero.event_name == "ClaimReferral";
    });

    let myclaimreferral = claimreferral.filter(function(hero) {
        return hero.result._user == (Address.uniq).toLowerCase();
    });

    let claimstaked = myevent.filter(function(hero) {
        return hero.event_name == "ClaimStaked";
    });

    let myclaimstake = claimstaked.filter(function(hero) {
        return hero.result._user == (Address.uniq).toLowerCase();
    });

    myreffarray = [];
    for (i = 0; i < myreferralreward.length; i++) {
        myreffarray.push(myreferralreward[i]);
    }
    for (i = 0; i < myclaimreferral.length; i++) {
        myreffarray.push(myclaimreferral[i]);
    }

    myarray = [];

    for (i = 0; i < mystake.length; i++) {
        myarray.push(mystake[i]);
    }
    for (i = 0; i < myunstake.length; i++) {
        myarray.push(myunstake[i]);
    }
    for (i = 0; i < myclaimstake.length; i++) {
        myarray.push(myclaimstake[i]);
    }
}

