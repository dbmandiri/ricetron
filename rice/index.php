<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,500,700,900" rel="stylesheet">

    <title>RICE Tron</title>
<!--
SOFTY PINKO
https://templatemo.com/tm-535-softy-pinko
-->

    <!-- Additional CSS Files -->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">

    <link rel="stylesheet" href="assets/css/templatemo-softy-pinko.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.9.0/dist/sweetalert2.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@10.9.0/dist/sweetalert2.min.css">
    </head>
    
    <body >
    
    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>  
    <!-- ***** Preloader End ***** -->
    
    
    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="main-nav">
                        <!-- ***** Logo Start ***** -->
                        <a href="#" class="logo">
                            <img src="assets/images/logorice.png" alt="Rice Tron" style="width: 30px">
                        </a>
                        <!-- ***** Logo End ***** -->
                        <!-- ***** Menu Start ***** -->
                        <ul class="nav">
                            <li><a href="#welcome" >Home</a></li>
                            <li><a href="#features">Whitepaper</a></li>
                            <li><a href="#features">JustSwap</a></li>
                        </ul>
                        <a class='menu-trigger'>
                            <span>Menu</span>
                        </a>
                        <!-- ***** Menu End ***** -->
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->

    <!-- ***** Welcome Area Start ***** -->
    <div class="welcome-area" id="welcome" >

        <!-- ***** Header Text Start ***** -->
        <div class="header-text">
            <div class="container">
                <div class="row justify-content-center">
                    <div class=" col-xl-8 col-lg-8 col-md-12 col-sm-12">
                        
                        <div class="mini  container">                      
                        <div class="mini-content" >

                            <div class="row">
                                <div class="col-12" style="padding-top: 50px">
                                    <h1>STAKE TRX TO MINE <strong>RET</strong></h1>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                                    <a href="#" class="mini-box">
                                        <i><img src="assets/images/icons8-safe-96.png" alt=""></i>
                                        <span>Total Stake</span>
                                        <strong id="totalstake">- TRX</strong>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                                    <a href="#" class="mini-box">
                                        <i><img src="assets/images/stair.png" alt=""></i>
                                        <span>Mining Level</span>
                                        <strong id="mininglevel">-</strong>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-12">
                                    <a href="#" class="mini-box">
                                        <i><img src="assets/images/value.png" alt=""></i>
                                        <span>Token Price</span>
                                        <strong id="retprice">-</strong>
                                    </a>
                                </div>
                                               
                            </div>
                        </div>

                    </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- ***** Header Text End ***** -->
    </div>
    <!-- ***** Welcome Area End ***** -->

    <!-- ***** Features Small Start ***** -->
    <section class="section home-feature" style="padding-top: 50px">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="row justify-content-center">

                        <!-- ***** Features Small Item Start ***** -->
                        <div class="col-lg-4 col-md-4 col-sm-6 col-12" data-scroll-reveal="enter bottom move 50px over 0.6s after 0.4s">
                            <div class="features-small-item">
                                <div class="icon">
                                    <i><img src="assets/images/icons8-card-wallet-96.png" style="width: 40px" alt=""></i>
                                </div>
                                <p>Your Wallet</p>
                                <h5 class="features-title" id="MyAddress">-</h5>
                                <br>
                            </div>
                            
                        </div>
                        <!-- ***** Features Small Item End ***** -->

                        <!-- ***** Features Small Item Start ***** -->
                        <div class="col-lg-4 col-md-4 col-sm-6 col-12" data-scroll-reveal="enter bottom move 50px over 0.6s after 0.6s">
                            <div class="features-small-item">
                                <div class="icon">
                                    <i><img src="assets/images/icons8-coin-wallet-96.png" style="width: 40px" alt=""></i>
                                </div>
                                <p>Your Balance</p>
                                <h5 class="features-title" ><span id="mybalance">TRX </span><br> <span id="mytokenbalance">RET </span></h5>
                                
                            </div>
                        </div>
                        <!-- ***** Features Small Item End ***** -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Features Small End ***** -->

    <!-- ***** Pricing Plans Start ***** -->
    <section class="section" id="pricing-plans">
        <div class="container">

            <div class="row justify-content-center">
                <!-- ***** Pricing Item Start ***** -->
                <div class="col-lg-6 col-md-6 col-sm-12" data-scroll-reveal="enter bottom move 50px over 0.6s after 0.2s">
                    <div class="pricing-item">                        
                        
                        <div class="pricing-body" >
                            <div class="price-wrapper">
                                <span class="price">Stake</span>
                                <span class="period">Stake TRX to get RET</span>
                            </div>
                            
                            <ul class="list">
                                <li class="active" style="font-size: 20px; padding-top: 30px">Staking Calculator:</li>
                                <div class="contact-form container">
                                <form id="contact" action="" method="get">
                                  <div class="row justify-content-center">
                                    <div class="col-lg-8 col-md-8 col-sm-10">
                                      <fieldset >
                                        <input name="trxamount" type="number" class="form-control" id="trxamount"  placeholder="Enter TRX Amount" required="" oninput="stakecalc(this.value)">
                                      </fieldset>
                                    </div>
                                  </div>
                                </form>
                            </div>
                                <li class="active">You will get:</li>
                            </ul>
                            <div class="row justify-content-center">
                                <div class="col-md-10 col-lg-8">
                                    <div class="price-wrapper ">
                                        <span class="price" style="font-size: 20px" id="onhour">RET</span>
                                        <span class="period" >Per Hour</span>
                                    </div>
                                    <div class="price-wrapper ">
                                        <span class="price" style="font-size: 20px" id="onday">RET</span>
                                        <span class="period" >Per Days</span>
                                    </div>
                                    <div class="price-wrapper ">
                                        <span class="price" style="font-size: 20px" id="onmax">RET</span>
                                        <span class="period" >Max</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="pricing-footer">
                            <button class="main-button" onclick="stake()">Stake Now</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12" data-scroll-reveal="enter bottom move 50px over 0.6s after 0.2s">
                    <div class="pricing-item">                        
                        
                        <div class="pricing-body">
                            <div class="price-wrapper">
                                <span class="price">Dashboard</span>
                                <span class="period">Mining Dashboard</span>
                            </div>
                    <div class="mini  container">                      
                        <div class="mini-content" >
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                    <a href="#" class="mini-box">
                                        <i><img src="assets/images/icons8-deposit-96.png" style="width: 40px" alt=""></i>
                                        <span>Active Stake</span>
                                        <strong><p id="activestake">-</p>TRX</strong>
                                    </a>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                    <a href="#" class="mini-box">
                                        <i><img src="assets/images/icons8-get-cash-96.png" style="width: 40px" alt=""></i>
                                        <span>RET Balance</span>
                                        <strong><p id="ricbalance2">-</p>RET</strong>
                                    </a>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                    <a href="#" class="mini-box">
                                        <i><img src="assets/images/icons8-withdrawal-96.png" style="width: 40px" alt=""></i>
                                        <span>Ready to claim</span>
                                        <strong><p id="readyclaim">-</p>RET</strong>
                                    </a>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-12" onclick="stakerecord()">
                                    <a href="#" class="mini-box">
                                        <i><img src="assets/images/icons8-order-history-96.png" style="width: 40px" alt=""></i>
                                        <span>History</span>
                                        <strong><p id="readyclaim"><br><br></p></strong>
                                    </a>
                                </div>
                                               
                            </div>
                        </div>

                    </div>

                </div>
                <div class="pricing-footer">
                            <button href="#" class="main-button" onclick="claim()">Claim RET</button>
                            <button class="main-button" onclick="unstake()">Unstake</button>
                        </div>
                </div>
                </div>

                <!-- ***** Pricing Item End ***** -->
            </div>
        </div>
    </section>
    <!-- ***** Pricing Plans End ***** -->
    <!-- ***** Pricing Plans Start ***** -->
    <section class="section" id="pricing-plans">
        <div class="container">

            <div class="row justify-content-center">
                <!-- ***** Pricing Item Start ***** -->
                <div class="col-lg-6 col-md-6 col-sm-12" data-scroll-reveal="enter bottom move 50px over 0.6s after 0.2s">
                    <div class="pricing-item">                        
                        
                        <div class="pricing-body container">
                            <div class="price-wrapper">
                                <span class="price">Referral Program</span>
                            </div>
                            <ul class="list container" style="padding-top: 20px">
                                
                                <li class="active">You can earn RET by inviting new users to the project get 10% RET from the total RET mined by your referrals</li>
                                <li class="active" style="font-size: 15px;" id="linknoted">Your Link : </li>
                            </ul>
                            <div class="row justify-content-center">
                                <div class="col-md-10 col-lg-10">
                                    <div class="price-wrapper " onclick="copylink();">
                                        <span class="period" id="refflink" style="font-size: 11px;">htps://ricetron.com/</span>
                                    </div>

                                    <ul class="list container" style="padding-top: 20px">
                                <li class="active" style="font-size: 20px;" >Your Reward:</li>
                            </ul>
                                    <div class="price-wrapper ">
                                        <span class="price" style="font-size: 20px" id="myreward">RET</span>
                                        <span class="period" id="myreferral">0 Referral</span>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>

                        <div class="pricing-footer">
                            <button class="main-button" onclick="claimreffreward()">Claim Reward</button>
                        </div>
                    </div>
                </div>

                <!-- ***** Pricing Item End ***** -->
            </div>
        </div>
    </section>
    <!-- ***** Pricing Plans End ***** -->
    

    <!-- ***** Blog Start ***** -->
    <section class="section" id="blog">
        <div class="container">
            <!-- ***** Section Title Start ***** -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="center-heading">
                        <h2 class="section-title">RICE TRON PLATFORM</h2>
                    </div>
                </div>
            </div>
            <!-- ***** Section Title End ***** -->

            <div class="row justify-content-center">
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="blog-post-thumb">
                        <div class="img">
                            <img src="assets/images/blog-item-01.png" alt="">
                        </div>
                        <div class="blog-content">
                            <h3>
                                <a href="#">RICE STAKING</a>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="blog-post-thumb">
                        <div class="img">
                            <img src="assets/images/blog-item-01.png" alt="">
                        </div>
                        <div class="blog-content">
                            <h3>
                                <a href="#">RICE DEFI</a>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Blog End ***** -->

    
    <!-- ***** Footer Start ***** -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="copyright">Copyright &copy; 2020 RICETRON</p>
                </div>
            </div>
        </div>
    </footer>
    
    <!-- jQuery -->
    <script src="assets/js/jquery-2.1.0.min.js"></script>
    <script src="assets/js/momment.js"></script>
    <script src="template/TronWeb.js"></script>

    <script type="text/javascript">


        const HttpProvider = TronWeb.providers.HttpProvider;
        const fullNode = new HttpProvider("https://api.trongrid.io");
        const solidityNode = new HttpProvider("https://api.trongrid.io");
        const eventServer = new HttpProvider("https://api.trongrid.io");

        var NoUp = 'TQuAji4sB1Fzpuj3dQH8ABCqLiiNBu6nu4';
        var contractAddress = 'TPBRQ9AqBpKctTyJ3RmAcobW8cNAFXF4SB';
        var params = new URLSearchParams(window.location.search);
        var refid = params.get('ref');
        var upline = refid;
        var trx_bal = 0;
        var ric_bal = 0;
        if(refid!==null){
            upline = refid;             
        }else{
            upline = NoUp;
        }
        
        
        var Address = {};
        function inittronlink(){
            if(window.tronWeb && window.tronWeb.defaultAddress.base58){
                Address.address = window.tronWeb.defaultAddress.base58;
                //alert(Address.address.hex[4]);
                let myadr = Address.address.slice(0, 5) + "xxxxx" + Address.address.slice(29, 34);
                   document.getElementById("MyAddress").innerHTML = myadr;                                   
            }
        }
        setInterval(myMethod1, 2000);
        function myMethod1( )
        {
          inittronlink();
          gethex();
        }
        setInterval(myMethod2, 2000);

        function myMethod2()
        {
           userinfo();
           stats();
           
        }
        async function getBalance() {
            const bal = await tronWeb.trx.getBalance(Address.address);
            Address.balance = (bal/1000000).toFixed(2);
            trx_bal = Address.balance;
            document.getElementById("mybalance").innerHTML = Address.balance + " TRX";
        }
        var lastclaim = 0;
        async function userinfo(){
            let instance = await tronWeb.contract().at(contractAddress);
            let userinfo = await instance.user().call();
            let txt = JSON.stringify(userinfo);
            let parse = JSON.parse(txt);
            let active_stake = tronWeb.fromSun(tronWeb.toDecimal(parse[0]._hex));
            lastclaim = tronWeb.address.fromHex(parse[1]);
            if(lastclaim!=0 || lastclaim > 0){
                document.getElementById("refflink").innerHTML = "https://ricetron.com/?ref="+Address.address;
                document.getElementById("linknoted").innerHTML = "Your Link , Click to copy !";
            }
            let referrer = tronWeb.address.fromHex(parse[3]);
            let referralReward = tronWeb.fromSun(tronWeb.toDecimal(parse[4]._hex)/1000000);
            let totalReferrals = tronWeb.toDecimal(parse[5]._hex);
            let pendingRewards = (tronWeb.fromSun(tronWeb.toDecimal(parse[6]._hex)/1000000)*1).toFixed(8);
            let tokenBalance = (tronWeb.fromSun(tronWeb.toDecimal(parse[7]._hex)/1000000)*1).toFixed(8);
            let balance = tronWeb.fromSun(tronWeb.toDecimal(parse[8]._hex));
            document.getElementById("mybalance").innerHTML =  + balance + " TRX";
            document.getElementById("mytokenbalance").innerHTML = tokenBalance + " RET";
            document.getElementById("myreward").innerHTML = referralReward + " RET"; 
            document.getElementById("myreferral").innerHTML = totalReferrals + " Referrals";
            document.getElementById("activestake").innerHTML = active_stake  ; 
            document.getElementById("ricbalance2").innerHTML = tokenBalance  ;
            document.getElementById("readyclaim").innerHTML = pendingRewards  ;        
        }
        
        var myarray = [];
        var myreffarray = [];
        var ricprice = 0 ;
        var currentLevel = 0 ;
        async function stats(){
            let instance = await tronWeb.contract().at(contractAddress);
            let userinfo = await instance.stats().call();
            let txt = JSON.stringify(userinfo);
            let parse = JSON.parse(txt);
            currentLevel = tronWeb.toDecimal(parse.currentLevel._hex);  
            let currentLevelYield = tronWeb.toDecimal(parse.currentLevelYield._hex)/100; // price per trx 
            ricprice = currentLevelYield / 10000000000;
            let currentLevelSupply = tronWeb.fromSun(tronWeb.toDecimal(parse.currentLevelSupply._hex))/1000000;
            let mintedCurrentLevel = tronWeb.fromSun(tronWeb.toDecimal(parse.mintedCurrentLevel._hex))/1000000;
            let totalRET = tronWeb.fromSun(tronWeb.toDecimal(parse.totalRET._hex))/1000000;
            let totalStaked = tronWeb.fromSun(tronWeb.toDecimal(parse.totalStaked._hex));
            document.getElementById("retprice").innerHTML =  ricprice + "TRX"; 
            document.getElementById("totalstake").innerHTML =  totalStaked + " TRX"; 
            document.getElementById("mininglevel").innerHTML =  (currentLevel+1) + " / 7";           
            //console.log(totalStaked);    

            let events = await tronWeb.event.getEventsByContractAddress(contractAddress, {
                    sort: 'block_timestamp',
                    only_confirmed: true
                });

                console.log(events);
                
                let stake =  events.filter(function(hero) {
                    return hero.result._type == "stake";
                });
                
                let mystake =  stake.filter(function(hero) {
                    return hero.result._user == (Address.uniq).toLowerCase();
                });
                
                let unstake =  events.filter(function(hero) {
                    return hero.result._type == "unstake";
                });
                
                let myunstake =  unstake.filter(function(hero) {
                    return hero.result._user == (Address.uniq).toLowerCase();
                });
                
                let referralreward =  events.filter(function(hero) {
                    return hero.name == "RefferalReward";
                });
                
                let myreferralreward =  referralreward.filter(function(hero) {
                    return hero._referrer == (Address.uniq).toLowerCase();
                });
                
                let claimreferral =  events.filter(function(hero) {
                    return hero.name == "ClaimReferral";
                });
                
                let myclaimreferral =  claimreferral.filter(function(hero) {
                    return hero.result._user == (Address.uniq).toLowerCase();
                });
                
                let claimstaked =  events.filter(function(hero) {
                    return hero.name == "ClaimStaked";
                });
                
                let myclaimstake =  claimstaked.filter(function(hero) {
                    return hero.result._user == (Address.uniq).toLowerCase();
                });
            
            myreffarray = [];
            for (i = 0; i < myreferralreward.length; i++) {
                myreffarray.push(mystake[i]);
            }for (i = 0; i < myreferralreward.length; i++) {
                myreffarray.push(mystake[i]);
            }
            
            myarray = [];
            
            for (i = 0; i < mystake.length; i++) {
                myarray.push(mystake[i]);
            }for (i = 0; i < myunstake.length; i++) {
                myarray.push(myunstake[i]);
            }for (i = 0; i < myclaimstake.length; i++) {
                myarray.push(myclaimstake[i]);
            }
            
            
                        
        }
        
        function stakerecord(){
            var staketable = makeTableHTML(myarray);
            openevents(staketable,"Stake");
        }
        
        function reffrecord(){
            var refftable = makeTableHTML(myreffarray);
            openevents(refftable,"Referral");
        }
        
        function makeTableHTML(myArray) {
            let result = '<table class="table table-borderless table-responsive"><thead><tr><th scope="col">type</th><th scope="col">Date</th><th scope="col">Amount</th></tr></thead><tbody><tr>';
            for(var i=0; i<myArray.length; i++) {
                result += "<tr>";
                    
                    
                    let tbody =myArray[i];
                    let Opr = tbody.name;
                    if(Opr == 'Operation'){
                        Opr = tbody.result._type;
                    }
                    
                    result += "<td>"+Opr+"</td>";
                    result += "<td>"+timetodate(tbody.timestamp)+"</td>";
                    result += "<td>"+tbody.result._amount+"</td>";
                    
                result += "</tr>";
            }
            result += "</table>";
        
            return result;
        }
       function openevents(a,b){
    Swal.fire({
  title: '<strong> '+b+' record</strong>',
  html:a,
  showCancelButton: false,
  focusConfirm: false,
  confirmButtonText:
    '<i class="fa fa-times"></i> Close',
  confirmButtonAriaLabel: 'Thumbs up, great!'
})
    
}
function timetodate(t){
    return moment(t).format("DD-MM-YYYY h:mm:ss");
}
        stats();

       function tohex(s){
            return tronWeb.toHex(s);
       }



        function uniq(data,key){
            return[
            ...new Map(
                data.map(x=>[key(x),x])
                ).values()
            ]
        }

        function stakecalc(val){
            let onday = ricprice * val ;
            let onhour = onday / 24;
            var onweek = onday * 7;
            let minig_duration = currentLevel * 1;

            if(currentLevel<1){
                onweek = onday * 3;
            }
            console.log(currentLevel);
            document.getElementById("onhour").innerHTML =  onhour.toFixed(8) + " RET"; 
            document.getElementById("onday").innerHTML =  onday.toFixed(8) + " RET"; 
            document.getElementById("onmax").innerHTML =  onweek.toFixed(8) + " RET"; 
        }
        function stake(){
         let depo = true;
             if (window.tronWeb && window.tronWeb.defaultAddress.base58) {
                 var obj = setInterval(async ()=>{
                        clearInterval(obj);
                        val = document.getElementById("trxamount").value ; 
                        if(val<100 || val === null){amounterror();depo=false;}else{              
                                let instance = await tronWeb.contract().at(contractAddress);
                                let result = await instance.stake(upline).send({
                                        feeLimit:100000000,// The maximum SUN consumes by calling this contract method.Hard capped at 1000 TRX.(1TRX = 1,000,000SUN)
                                        callValue:val*1000000,//in SUN. 1 TRX = 1,000,000 SUN
                                      shouldPollResponse:true
                                    });
                                console.log(result);
                        }
                }, 10)
             }else{signin();}
      }
      function unstake(){
        Swal.fire({
          title: 'Request Confirmation',
          text: "unstake will be charge 10% fee from your staking",
          icon: 'question',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Yes, Unstake!'
        }).then((result) => {
          if (result.isConfirmed) {
            let depo = true;
             if (window.tronWeb && window.tronWeb.defaultAddress.base58) {
                 var obj = setInterval(async ()=>{
                        clearInterval(obj);             
                                let instance = await tronWeb.contract().at(contractAddress);
                                let result = await instance.unstake().send({
                                        feeLimit:100000000,// The maximum SUN consumes by calling this contract method.Hard capped at 1000 TRX.(1TRX = 1,000,000SUN)
                                        //callValue:val*1000000,//in SUN. 1 TRX = 1,000,000 SUN
                                      shouldPollResponse:true
                                    });
                                console.log(result);
                        
                }, 10)
             }else{signin();}
          }
        })
      }
      function claim(){
       
             if (window.tronWeb && window.tronWeb.defaultAddress.base58) {
                 var obj = setInterval(async ()=>{
                        clearInterval(obj);             
                                let instance = await tronWeb.contract().at(contractAddress);
                                let result = await instance.claimStaked().send({
                                        feeLimit:100000000,// The maximum SUN consumes by calling this contract method.Hard capped at 1000 TRX.(1TRX = 1,000,000SUN)
                                        //callValue:val*1000000,//in SUN. 1 TRX = 1,000,000 SUN
                                      shouldPollResponse:true
                                    });           
                }, 10)
             }else{signin();}
          
      }
      function claimreffreward(){
       
             if (window.tronWeb && window.tronWeb.defaultAddress.base58) {
                 var obj = setInterval(async ()=>{
                        clearInterval(obj);             
                                let instance = await tronWeb.contract().at(contractAddress);
                                let result = await instance.claimReferralReward().send({
                                        feeLimit:100000000,// The maximum SUN consumes by calling this contract method.Hard capped at 1000 TRX.(1TRX = 1,000,000SUN)
                                        //callValue:val*1000000,//in SUN. 1 TRX = 1,000,000 SUN
                                      shouldPollResponse:true
                                    });
                                    Swal.fire(
      'Referral Reward Claimed!',
      'Referral successfully added to your wallet.',
      'success'
    )          
                }, 10)
             }else{signin();}
          
      }
      function amounterror(){
             Swal.fire({
              title: 'Error',
              text: 'Amount must be more or equal to 100 TRX.',
              icon: 'error',
              confirmButtonText: 'close'
            });
        }
        function signin(){
            if(window.tronWeb && window.tronWeb.defaultAddress.base58){}else{
             Swal.fire({
              title: 'Connection Request',
              text: "Please login to your Tronlink to connect with RICE Tron.",
              icon: 'warning',
              confirmButtonText: 'close'
            });
            }
        }
        function copylink(){
            if(lastclaim!=0){
            let reflink = "https://ricetron.com/?ref="+Address.address;
            copyToClipboard(reflink);
            Swal.fire({
              title: 'Success',
              text: 'Referral link was copied to clipboard.',
              icon: 'success',
              confirmButtonText: 'Close'
            });
            }else{
                Swal.fire({
              title: 'NO LINK',
              text: 'Please stake TRX first.',
              icon: 'warning',
              confirmButtonText: 'Close'
            });
            }
          
        }
         function copyToClipboard(text) {
          var input = document.body.appendChild(document.createElement("input"));
          input.value = text;
          input.select();
          document.execCommand('copy');
          input.parentNode.removeChild(input);
        };

        function gethex(){
        let url = 'hello.php?address=' + Address.address;
       
       let res = $.get(url); 
       $.get( url, function( data ) {
           
         Address.uniq =  "0x"+data.slice(2, 42)  ;
         
         console.log(Address.uniq);
         
        });

            
        }

    </script>

    <!-- Bootstrap -->
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- Plugins -->
    <script src="assets/js/scrollreveal.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/imgfix.min.js"></script> 
    
    <!-- Global Init -->
    <script src="assets/js/custom.js"></script>

  </body>
</html>