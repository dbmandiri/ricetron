
    
        const HttpProvider = TronWeb.providers.HttpProvider;
        const fullNode = new HttpProvider("https://api.trongrid.io");
        const solidityNode = new HttpProvider("https://api.trongrid.io");
        const eventServer = new HttpProvider("https://api.trongrid.io");

        var NoUp = 'TEURHfev67C4inLo25EuzoiUXAnxz8jUZn';
        var contractAddress = 'TQKnKYPcdYTGJdZN38QNaJ2RfkwSRZkULg';
        var params = new URLSearchParams(window.location.search);
        var refid = params.get('refid');
        var total_invested = 0 ;
        if(refid!==null){
                var upline = refid;
             
        }else{
            var upline = NoUp;
        }
        
        function signin(){
            if(window.tronWeb && window.tronWeb.defaultAddress.base58){}else{
             Swal.fire({
              title: 'Connection Request',
              text: "Please connect using Tronlink to continue.",
              icon: 'warning',
              confirmButtonText: 'close'
            });
            }
        }
        function amounterror(){
             Swal.fire({
              title: 'Error',
              text: 'Amount must be more or equal to 50 TRX.',
              icon: 'error',
              confirmButtonText: 'close'
            });
        }
        setInterval(myMethod, 5000);
        function myMethod( )
        {
          inittronlink();
          contractinfo();
        }
        function inittronlink(){
                if(window.tronWeb && window.tronWeb.defaultAddress.base58){
                    Address.address = window.tronWeb.defaultAddress.base58;
                   document.getElementById("yourAddress").innerHTML = window.tronWeb.defaultAddress.base58;
                   document.getElementById("yourAddress1").innerHTML = window.tronWeb.defaultAddress.base58;
                   document.getElementById("yourAddress2").innerHTML = window.tronWeb.defaultAddress.base58;
                   document.getElementById("yourAddress3").innerHTML = window.tronWeb.defaultAddress.base58;
                   document.getElementById("yourAddress4").innerHTML = window.tronWeb.defaultAddress.base58;
                  contractinfo();
                   getBalance();
                   userinfo();
                   //getEvent();
                   
                   
                }
            }
        inittronlink();
        function goContract(){
            window.open('https://tronscan.org/#/contract/TQKnKYPcdYTGJdZN38QNaJ2RfkwSRZkULg/code');
        }
        function goPaper(){
            window.open('https://troncycle.com/TRONCYCLE_LIGHTPAPER.pdf');
        }
        function opentelegram(){
            window.open('https://t.me/TronCycle'); 
        }
		async function getBalance() {
		    const bal = await tronWeb.trx.getBalance(Address.address);
		    Address.balance = (bal/1000000).toFixed(2);
		    document.getElementById("yourbalance").innerHTML = Address.balance + " TRX";
		}
		async function getEvent() {
		    let event = await tronWeb.getEventResult(contractAddress,{size:5});
		    let txtevent = JSON.stringify(event);
		    //console.log(event);
		    let eventlength = 5;
		    if(event.length < 5){
		        eventlength = event.length;
		    }
		    for (i = 0; i < eventlength; ++i) {
		        //alert(tronWeb.toAscii(event[i]['result'][0]))
		        //if(event[i]['result'][0]==Address.address){
    		        if(event[i]['name']=='NewDeposit'){
    		         document.getElementById("eventhistory"+i).innerHTML = tmstmp(event[i]['timestamp'])+" Dépôt "+(event[i]['result']['amount']/1000000)+" TRX";
    		        }else if(event[i]['name']=='Withdraw'){
    		         document.getElementById("eventhistory"+i).innerHTML = tmstmp(event[i]['timestamp'])+" Se désister "+(event[i]['result']['amount']/1000000)+" TRX";  
    		        }else if(event[i]['name']=='MatchPayout'){
    		         document.getElementById("eventhistory"+i).innerHTML = tmstmp(event[i]['timestamp'])+" Bonus de parrainage "+(event[i]['result']['amount']/1000000)+" TRX";   
    		        }else if(event[i]['name']=='Upline'){
    		         document.getElementById("eventhistory"+i).innerHTML = tmstmp(event[i]['timestamp'])+" Inviter "+(event[i]['result']['bonus']/1000000)+" TRX";  
    		        }
		        //}
            }
            
		}
		function tmstmp(t){
		   return  moment(t).format('L');
		}
      function invest(v,p){
         let depo = true;
    	     if (window.tronWeb && window.tronWeb.defaultAddress.base58) {
                 var obj = setInterval(async ()=>{
                        clearInterval(obj);
                        val = document.getElementById("amt"+v).value ; 
                        if(val<50 || val === null){amounterror();depo=false;}else{                        
                            if(depo==true){
                                let instance = await tronWeb.contract().at(contractAddress);
                                let result = await instance.deposit(p,upline).send({
                                	    feeLimit:100000000,// The maximum SUN consumes by calling this contract method.Hard capped at 1000 TRX.(1TRX = 1,000,000SUN)
                                	    callValue:val*1000000,//in SUN. 1 TRX = 1,000,000 SUN
                                	  shouldPollResponse:true
                                	});
                                console.log(result);
                                
                                document.getElementById("yourAddress").focus();
                            }
                        }
                }, 10)
    	     }else{signin();}
      }
	    function withdraw(){
	     if (window.tronWeb && window.tronWeb.defaultAddress.base58) {
             var obj = setInterval(async ()=>{
                    clearInterval(obj)
                    var tronweb = window.tronWeb;
                     let instance = await tronWeb.contract().at(contractAddress);
                        let result = await instance.withdraw().send({
                        	  shouldPollResponse:true
                        	});
                    console.log(result);
            }, 10)
	     }else{signin();}
        }
        async function contractinfo(){
            let instance = await tronWeb.contract().at(contractAddress);
            let info = await instance.totalInvested().call();
            let txtinfo = JSON.stringify(info);
            let parseinfo = JSON.parse(txtinfo);
            let info1 = await instance.totalRefRewards().call();
            let txtinfo1 = JSON.stringify(info1);
            let parseinfo1 = JSON.parse(txtinfo1);
            let info2 = await instance.totalInvestors().call();
            let txtinfo2 = JSON.stringify(info2);
            let parseinfo2 = JSON.parse(txtinfo2);

            document.getElementById("all_invest").innerHTML = (tronWeb.toDecimal(parseinfo._hex)/1000000).toFixed(0) + "TRX";
           document.getElementById("all_rewards").innerHTML = (tronWeb.toDecimal(parseinfo1._hex)/1000000).toFixed(0) + "TRX";
            
        }
        contractinfo();
        async function userinfo(){
          
            let instance = await tronWeb.contract().at(contractAddress);
            let userinfo = await instance.investors(Address.address).call();
            let txt = JSON.stringify(userinfo);
            let parse = JSON.parse(txt);
            var invested = parse.invested._hex;
             total_invested = (tronWeb.toDecimal(invested)/1000000);
            var referral_bonus = parse.totalRef._hex;
            var withdrawn = parse.withdrawn._hex;
            var before = withdrawn - referral_bonus;
            if(total_invested>0){
                    document.getElementById("refflink").innerHTML = "https://troncycle.com/?refid="+Address.address;
                    document.getElementById("linknoted").innerHTML = "Click to copy!";
            }

            document.getElementById("invested").innerHTML = total_invested.toFixed(2)+ " TRX" ;
            document.getElementById("withdrawn").innerHTML = (tronWeb.toDecimal(withdrawn)/1000000).toFixed(2)+ " TRX" ;
            document.getElementById("referral").innerHTML = (tronWeb.toDecimal(referral_bonus)/1000000).toFixed(2) + " TRX";  
            
            let wdable = await instance.withdrawable(Address.address).call();
            let txt1 = JSON.stringify(wdable);
            let parse1 = JSON.parse(txt1);
            var withdrawable = parse1.amount._hex ;
            document.getElementById("dividends").innerHTML = (tronWeb.toDecimal(withdrawable)/1000000)+ " TRX";

            let bonushold = await instance.getHoldBonus(Address.address).call();
            let txt2 = JSON.stringify(bonushold);
            let parse2 = JSON.parse(txt2);
            var holdbonus = parse2._hex;
            document.getElementById("holdbonus").innerHTML = (tronWeb.toDecimal(holdbonus)/1000000) + " TRX";  
            
            let bonusfund = await instance.getFundBonus().call();
            let txt3 = JSON.stringify(bonusfund);
            let parse3 = JSON.parse(txt3);
            var fundbonus = parse3._hex;
            document.getElementById("fundbonus").innerHTML = (tronWeb.toDecimal(fundbonus)/1000000) + " TRX";  
            
            let bonusreff = await instance.getRefBonus(Address.address).call();
            let txt4 = JSON.stringify(bonusreff);
            let parse4 = JSON.parse(txt4);
            var referralbonus = parse4._hex;
            document.getElementById("referralbonus").innerHTML = (tronWeb.toDecimal(referralbonus)/1000000) + " TRX";  
            
        
        }

        
        
        function copylink(){
            if(total_invested>0){
            let reflink = "https://troncycle.com/?refid="+Address.address;
            copyToClipboard(reflink);
            Swal.fire({
              title: 'Success',
              text: 'Referral link was copied to clipboard.',
              icon: 'success',
              confirmButtonText: 'Close'
            });
            }else{
                Swal.fire({
              title: 'NO LINK',
              text: 'Please make an investment first.',
              icon: 'warning',
              confirmButtonText: 'Close'
            });
            }
          
        }
        
        function banner1(){
            if(total_invested>0){
            let reflink = "'https://troncycle.com/?refid="+Address.address+"'";
            let txtbanner = '<img src="https://troncycle.com/banner.gif" alt="banner" style="width:506px;" onclick="window.open('+reflink+')">';
            copyToClipboard(txtbanner);
            Swal.fire({
              title: 'Success',
              text: 'Banner 1 code was copied to clipboard.',
              icon: 'success',
              confirmButtonText: 'Close'
            });
            }else{
                Swal.fire({
              title: 'NO LINK',
              text: 'Please make an investment first.',
              icon: 'warning',
              confirmButtonText: 'Close'
            });
            }
          
        }
        function bannersquare(){
            if(total_invested>0){
            let reflink = "'https://troncycle.com/?refid="+Address.address+"'";
            let txtbanner = '<img src="https://troncycle.com/banner1.gif" alt="banner2" style="width:300px;" onclick="window.open('+reflink+')">';
            copyToClipboard(txtbanner);
            Swal.fire({
              title: 'Success',
              text: 'Banner 2 code was copied to clipboard.',
              icon: 'success',
              confirmButtonText: 'Close'
            });
            }else{
                Swal.fire({
              title: 'NO LINK',
              text: 'Please make an investment first.',
              icon: 'warning',
              confirmButtonText: 'Close'
            });
            }
          
        }
        function copyToClipboard(text) {
          var input = document.body.appendChild(document.createElement("input"));
          input.value = text;
          input.select();
          document.execCommand('copy');
          input.parentNode.removeChild(input);
        };
        
        function setCookie(cname,cvalue,exdays) {
          var d = new Date();
          d.setTime(d.getTime() + (exdays*24*60*60*1000));
          var expires = "expires=" + d.toGMTString();
          document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }
        
        function getCookie(cname) {
          var name = cname + "=";
          var decodedCookie = decodeURIComponent(document.cookie);
          var ca = decodedCookie.split(';');
          for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
              c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
              return c.substring(name.length, c.length);
            }
          }
          return "";
        }
        
        function checkCookie() {
          var user=getCookie("agree");
          if (user != "") {
          } else {
             const swalWithBootstrapButtons = Swal.mixin({
                  customClass: {
                    confirmButton: 'btn btn-success'
                  },
                  buttonsStyling: false
                })
                
                swalWithBootstrapButtons.fire({
                  text: "By using our site you agree to our use of cookies to deliver a better site experience.",
                  
                  confirmButtonText: "ACCEPT"
                }).then((result) => {
                  if (result.value) {
                    setCookie("agree", "agree", 30);
                  }
                })
          }
        }
        checkCookie();
        
        