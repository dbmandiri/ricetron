<?php
    require_once('function_news.php');
    $id = $_GET['id'];

    $data = file_get_contents('http://rtlistnews.koppln.com/api/news-api/'.$id);
    $dataBerita = json_decode($data);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <base href="https://ricetron.koprener.com/news.php">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/images/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,500,700,900" rel="stylesheet">
    <title>Ricetron</title>
    <!-- Additional CSS Files -->
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="assets/css/modalxyz.css">
    <link rel="stylesheet" href="assets/css/templatemo-softy-pinko.css">
    <script src="assets/js/jquery-2.1.0.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"
        integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ=="
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.9.0/dist/sweetalert2.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@10.9.0/dist/sweetalert2.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/dataTables.foundation.min.css">
    <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.23/js/dataTables.foundation.min.js"></script>
    <script src="https://kit.fontawesome.com/766e1e1bca.js" crossorigin="anonymous"></script>

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '402993468093516');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=402993468093516&ev=PageView&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
</head>

<body>

    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>

    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="main-nav">
                        <a href="#" class="logo">
                            <img src="assets/images/logo.png" alt="Ricetron" style="width: 60px" />
                        </a>
                        <!-- ***** Logo End ***** -->
                        <!-- ***** Menu Start ***** -->
                        <ul class="nav">
                            <li><a href="../#welcome">Home</a></li>
                            <li><a href="../#about">About</a></li>
                            <li><a href="../#dashboard">Dashboard</a></li>
                            <li><a href="../#team">Team</a></li>
                            <li><a href="#"
                                    onclick="window.open('https://ricetron.com/RICETRON_WHITEPAPER_v5.pdf', '_blank')">Whitepaper</a>
                            </li>
                            <li><a href="#"
                                    onclick="window.open('https://ricetron.com/RICETRON_SC_AUDIT.pdf', '_blank')">Audit
                                    Report</a></li>
                            <li><a href="#"
                                    onclick="window.open('https://tronscan.org/#/contract/TPBRQ9AqBpKctTyJ3RmAcobW8cNAFXF4SB/code', '_blank')">Contract</a>
                            </li>
                            <!--li><a  data-toggle="modal" data-target="#SwapModal">Swap</a></li-->
                            <li><a href="#" onclick="#">RiceLend</a></li>
                            <li><a href="news-list.php">RiceNews</a></li>
                        </ul>
                        <a class='menu-trigger'>
                            <span>Menu</span>
                        </a>
                        <!-- ***** Menu End ***** -->
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <?php 
    if($dataBerita != null){
    ?>
    <section class="section" style="margin-top: 4%;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card bg-light">
                        <div class="card-body">
                            <h5 class="card-title"><?= $dataBerita->title; ?></h5>
                            <p class="card-text"><?= $dataBerita->headline; ?></p>
                        </div>
                        <img src="<?= $dataBerita->image; ?>" class="card-img-bottom">
                        <div class="card-body">
                            <p class="card-text"><?= $dataBerita->writer; ?> &nbsp&nbsp<small class="text-muted"><i
                                        class="far fa-clock"></i>&nbsp
                                    <?= date("D, F d Y h:m", strtotime($dataBerita->date_publish)); ?></small></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top : 2%">
                <div class="col-lg-12">
                    <p><?= $dataBerita->news_content; ?></p>
                </div>
            </div>
        </div>
    </section>
    <?php
    }
    else{
    ?>
    <section class="section" style="margin-top: 4%;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card bg-light">
                        <div class="card-body">
                            <h5 class="card-title">404 News Not Found</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top : 2%">
                <div class="col-lg-12">
                </div>
            </div>
        </div>
    </section>
    <?php
    }
    ?>
    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                </div>
            </div>
        </div>
    </section>
    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">

                </div>
            </div>
        </div>
    </section>


    <!-- ***** Footer Start ***** -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <ul class="social">
                        <li><a href="#" onclick="window.open('https://twitter.com/RicetronO', '_blank')"><i
                                    class="fa fa-twitter"></i></a></li>

                        <li><a href="#" onclick="window.open('https://instagram.com/ricetronofficial', '_blank')"><i
                                    class="fa fa-instagram"></i></a></li>
                        <li><a href="#" onclick="window.open('https://medium.com/ricetron', '_blank')"><i
                                    class="fa fa-medium"></i></a></li>
                        <li><a href="#"
                                onclick="window.open('https://www.youtube.com/channel/UCVctUuFg7ihopgmqNPMm4bA', '_blank')"><i
                                    class="fa fa-youtube"></i></a></li>

                        <li><a href="#" onclick="window.open('http://T.me/ricetron', '_blank')"><i
                                    class="fa fa-send"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p class="copyright">Copyright &copy; 2021 Ricetron </p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->

    <!-- Bootstrap -->
    <script src="assets/js/popper.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Plugins -->

    <script src="template/TronWeb.js"></script>
    <script src="assets/js/scrollreveal.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/imgfix.min.js"></script>
    <script src="assets/js/chart.js"></script>
    <script>
        function info() {
            Swal.fire({
                icon: 'info',
                title: 'Maintenance Info',
                text: 'Dear all Ricetron users, maintenance will be carried out on February 17 at 00:00 to 09:00. Please claim your RET, Unstake your TRX, and dont stake first during maintenance!',
                footer: 'Regards, Ricetron Team.',
                showConfirmButton: false
            })
        }
        var element = document.getElementById("modalxyz");

        function openpopup() {
            element.classList.add("active");
        }

        function closepopup() {
            element.classList.remove("active");
        }
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })

        var allowbal = 0;

        async function allowance() {
            let instance = await tronWeb.contract().at('TGFLaoFoS5RwdNiarqwDhJ89v9S6g5snKP');
            let userinfo = await instance.allowance(Address.address, 'TXArjwfQ5LVzBcGAKWrt9S2g3wMhm29Ude').call();
            let txt = JSON.stringify(userinfo);
            let parse = JSON.parse(txt);
            //;
            allowbal = tronWeb.toDecimal(parse._hex);
        }
        async function usablebal() {
            let instance = await tronWeb.contract().at('THmDwkSd9dpkMqsUbMLnU3LVVzQMGG9rvA');
            let userinfo = await instance.balanceOf(Address.address).call();
            let txt = JSON.stringify(userinfo);
            let parse = JSON.parse(txt);

            document.getElementById("RETUSABLE").innerHTML = ((tronWeb.toDecimal(parse._hex) / 10e11) * 1).toFixed(
                8) + "RET";
        }

        async function approve() {
            if (window.tronWeb && window.tronWeb.defaultAddress.base58) {
                var obj = setInterval(async () => {
                    clearInterval(obj);
                    let instance = await tronWeb.contract().at('TGFLaoFoS5RwdNiarqwDhJ89v9S6g5snKP');
                    let result = await instance.approve('TXArjwfQ5LVzBcGAKWrt9S2g3wMhm29Ude',
                        '115792089237316195423570985008687907853269984665640564039457584007913129639935'
                    ).send({
                        feeLimit: 100000000, // The maximum SUN consumes by calling this contract method.Hard capped at 1000 TRX.(1TRX = 1,000,000SUN)
                        //callValue:val*1000000,//in SUN. 1 TRX = 1,000,000 SUN
                        shouldPollResponse: true
                    });
                    console.log(result);
                }, 10)
            }
        }
        async function swap() {
            allowance();
            if (allowbal > 0) {
                if (window.tronWeb && window.tronWeb.defaultAddress.base58) {
                    var obj = setInterval(async () => {
                        clearInterval(obj);
                        let instance = await tronWeb.contract().at(
                            'TXArjwfQ5LVzBcGAKWrt9S2g3wMhm29Udezz');
                        let result = await instance.SwapRET(RETBAL).send({
                            feeLimit: 100000000, // The maximum SUN consumes by calling this contract method.Hard capped at 1000 TRX.(1TRX = 1,000,000SUN)
                            //callValue:val*1000000,//in SUN. 1 TRX = 1,000,000 SUN
                            shouldPollResponse: true
                        });
                        console.log(result);
                    }, 10)
                }
            } else {
                Swal.fire({
                    title: 'Info',
                    text: "You need to approve before swap / transfer Staking Balance to Usable Balance.",
                    icon: 'info',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Approve'
                }).then((result) => {
                    if (result.isConfirmed) {
                        approve();
                    }
                })
            }
        }
    </script>

    <!-- Global Init -->
    <script src="assets/js/custom.js"></script>
</body>

</html>