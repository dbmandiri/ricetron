<?php
function time_elapsed_string($datetime, $full = false) {
    if($datetime != null){
        $current = strtotime(date("Y-m-d"));
        $date    = strtotime($datetime);

        $datediff = $date - $current;
        $difference = floor($datediff/(60*60*24));
        if($difference <= -7){
            $newDate = date("d-F-Y", strtotime($datetime));
            return $newDate;
        }
        else{
            $now = new DateTime;
            $ago = new DateTime($datetime);
            $diff = $now->diff($ago);

            $diff->w = floor($diff->d / 7);
            $diff->d -= $diff->w * 7;
        
            $string = array(
                'y' => 'year',
                'm' => 'month',
                'w' => 'week',
                'd' => 'day',
                'h' => 'hour',
                'i' => 'minute',
                's' => 'second',
            );
            foreach ($string as $k => &$v) {
                if ($diff->$k) {
                    $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
                } else {
                    unset($string[$k]);
                }
            }
        
            if (!$full) $string = array_slice($string, 0, 1);
            return $string ? implode(', ', $string) . ' ago' : 'just now';
        }
    }
}
?>